$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/functionalTests/End2End_Tests.feature");
formatter.feature({
  "name": "Automated End2End Tests",
  "description": "  Description: The purpose of this feature is to test End 2 End test.",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User can login successfully",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user is at login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_is_at_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "login with email \"anh.tran.3889@gmail.com\" and password \"quangteo\"",
  "keyword": "When "
});
formatter.match({
  "location": "LoginPageSteps.login_with_email_and_password(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "he can login successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.he_can_login_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "User create a new note",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user is at login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_is_at_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "login with email \"anh.tran.3889@gmail.com\" and password \"quangteo\"",
  "keyword": "When "
});
formatter.match({
  "location": "LoginPageSteps.login_with_email_and_password(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "select the org \"My LG\"",
  "keyword": "And "
});
formatter.match({
  "location": "OrgListPageSteps.login_with_email_and_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "select roadmap \"Test\"",
  "keyword": "And "
});
formatter.match({
  "location": "RoadMapsPageSteps.select_roadmap(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "create a new note \"Test note\"",
  "keyword": "And "
});
formatter.match({
  "location": "RoadMapsPageSteps.create_a_new_note(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the note is created successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "RoadMapsPageSteps.the_note_is_created_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});