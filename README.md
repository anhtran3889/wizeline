---

## Project notes:

- Project is implemented with Cucumber + Selenium + Maven + Java
- Please use Chrome version 73 if you want to run it
- Please view the test cases in file src/test/resources/functionalTests/End2End_Tests.feature
- Running the test using the command: mvn install (first time) / mvn test
- The html report is located at target/cucumber-html-reports

---