package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import selenium.Wait;

public class OrgListPage {

	WebDriver driver;
	
	public OrgListPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectOrg(String name) {
		driver.findElement(By.xpath("//span[text()='" + name + "']")).click();
		Wait.untilPageLoadComplete(driver);
	}
}
