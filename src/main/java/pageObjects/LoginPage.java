package pageObjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import managers.FileReaderManager;
import selenium.Wait;

public class LoginPage {

	WebDriver driver;
	
	public LoginPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@name='email']")
	private WebElement txtEmail;
	
	@FindBy(how = How.XPATH, using = "//input[@name='password']")
	private WebElement txtPassword;
	
	@FindBy(how = How.CSS, using = ".track-submit-login")
	private WebElement btnLogin;
	
	public void enterEmail(String email) {
		txtEmail.sendKeys(email);
	}
	
	public void enterPassword(String password) {
		txtPassword.sendKeys(password);
	}
	
	public void clickOnLogin() {
		btnLogin.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.untilPageLoadComplete(driver);
	}
	
	public void navigateToLogin() {
		driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
		Wait.untilPageLoadComplete(driver);
	}
	
	public void login(String email, String password) {
		enterEmail(email);
		enterPassword(password);
		clickOnLogin();
	}
	
	public Boolean isLoggedIn() {
		String url = driver.getCurrentUrl();
		return url.equals("https://login.wizelineroadmap.com/organizations-list");
	}
	
}