package pageObjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class RoadMapsPage {

	WebDriver driver;
	
	public RoadMapsPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.CSS, using = ".emptyRowHoverArea")
	private WebElement emptyRow;
	
	@FindBy(how = How.XPATH, using = "//input[@name='name']")
	private WebElement txtName;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Done']")
	private WebElement btnDone;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(), 'Item created succesfully')]")
	private WebElement msgSuccess;
	
	public void selectRoadMap(String roadmap) {
		driver.findElement(By.xpath("//span[text()='" + roadmap + "']")).click();
	}
	
	public void clickOnEmptyRow() {
		emptyRow.click();
	}
	
	public void enterNameNote(String name) {
		txtName.sendKeys(name);
	}
	
	public void clickOnDone() {
		btnDone.click();
	}
	
	public void addNote(String note) {
		clickOnEmptyRow();
		enterNameNote(note);
		clickOnDone();
	}
	
	public Boolean isSuccessMessageDisplayed() {
		return msgSuccess.isDisplayed();
	}
	

}
