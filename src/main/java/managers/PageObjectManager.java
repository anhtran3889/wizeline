package managers;

import org.openqa.selenium.WebDriver;

import pageObjects.OrgListPage;
import pageObjects.RoadMapsPage;
import pageObjects.LoginPage;

//This class is used to manage all page object classes
public class PageObjectManager {
	private WebDriver driver;
	private LoginPage homePage;
	private OrgListPage orgListPage;
	private RoadMapsPage roadMapsPage;
	
	public PageObjectManager(WebDriver driver) {
		this.driver = driver;
	}

	public LoginPage getLoginPage() {
		return (homePage == null) ? homePage = new LoginPage(driver) : homePage;
	}
	
	public OrgListPage getOrgListPage() {
		return (orgListPage == null) ? orgListPage = new OrgListPage(driver) : orgListPage;
	}
	
	public RoadMapsPage getRoadMapsPage(){
		return (roadMapsPage == null) ? roadMapsPage = new RoadMapsPage(driver) : roadMapsPage;
	}
	
}
