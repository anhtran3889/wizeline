package stepDefinitions;

import cucumber.TestContext;
import cucumber.api.java.en.When;
import pageObjects.OrgListPage;

public class OrgListPageSteps {

	TestContext testContext;
	OrgListPage orgListPage;
	
	public OrgListPageSteps(TestContext context) {
		testContext = context;
		orgListPage = testContext.getPageObjectManager().getOrgListPage();
	}
	
	@When("select the org {string}")
	public void login_with_email_and_password(String org) {
	    orgListPage.selectOrg(org);
	}
	
}
