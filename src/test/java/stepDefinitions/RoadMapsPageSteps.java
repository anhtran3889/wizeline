package stepDefinitions;

import org.junit.Assert;

import cucumber.TestContext;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.RoadMapsPage;

public class RoadMapsPageSteps {
	
	TestContext testContext;
	RoadMapsPage roadMapsPage;
	
	public RoadMapsPageSteps(TestContext context) {
		testContext = context;
		roadMapsPage = testContext.getPageObjectManager().getRoadMapsPage();
	}
	
	@When("select roadmap {string}")
	public void select_roadmap(String roadmap) {
		roadMapsPage.selectRoadMap(roadmap);
	}
	
	@When("create a new note {string}")
	public void create_a_new_note(String note) {
	    roadMapsPage.addNote(note);
	}
	
	@Then("the note is created successfully")
	public void the_note_is_created_successfully() {
	    Assert.assertTrue(roadMapsPage.isSuccessMessageDisplayed());
	}
}
