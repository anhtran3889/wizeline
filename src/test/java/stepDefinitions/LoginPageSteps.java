package stepDefinitions;

import org.junit.Assert;

import cucumber.TestContext;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.LoginPage;

public class LoginPageSteps {

	TestContext testContext;
	LoginPage loginPage;
	
	public LoginPageSteps(TestContext context) {
		testContext = context;
		loginPage = testContext.getPageObjectManager().getLoginPage();
	}
	
	@Given("user is at login page")
	public void user_is_at_login_page() {
		loginPage.navigateToLogin();
	}

	@When("login with email {string} and password {string}")
	public void login_with_email_and_password(String email, String password) {
	    loginPage.login(email, password);
	}
	
	@Then("he can login successfully")
	public void he_can_login_successfully() {
	    Assert.assertTrue(loginPage.isLoggedIn());
	}
}
