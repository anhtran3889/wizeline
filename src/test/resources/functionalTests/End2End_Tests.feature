Feature: Automated End2End Tests
  Description: The purpose of this feature is to test End 2 End test.

  Scenario: User can login successfully
    Given user is at login page
    When login with email "anh.tran.3889@gmail.com" and password "quangteo"
    Then he can login successfully

  Scenario: User create a new note
    Given user is at login page
    When login with email "anh.tran.3889@gmail.com" and password "quangteo"
    And select the org "My LG"
    And select roadmap "Test"
    And create a new note "Test note"
    Then the note is created successfully
